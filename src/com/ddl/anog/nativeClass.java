package com.ddl.anog;

import android.content.res.AssetManager;

public class nativeClass {

	static {
		System.loadLibrary("nativeclass");
	}
	
	public static native void drawFrame();
	public static native void update(float delta);
	public static native void reinit(int w,int h);
	public static native void setup();
	public static native void setAssetsManager(AssetManager mngr);
	
	//touch
	public static native void touchDown(int id, float x,float y);
	public static native void touchUp(int id, float x,float y);
	public static native void touchMove(int id, float x,float y);
	
}
