package com.ddl.anog;

import java.util.Timer;
import java.util.TimerTask;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.SystemClock;
import android.util.Log;


public class renderer implements GLSurfaceView.Renderer {
	public static Activity activity;
	public Timer timer;
	public boolean scheduled = false; 

	public renderer(Activity act) {
		activity = act;
		//timer = new Timer();
		
	}

	@Override
	public void onDrawFrame(GL10 gl) {
		//if (!scheduled)
		//{
		//	scheduled = true;
		//	timer.schedule(new updateTask(),0,20);
		//}
		nativeClass.update(0);
		nativeClass.drawFrame();
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		nativeClass.reinit(width, height);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		nativeClass.setup();	
	}
	
}



