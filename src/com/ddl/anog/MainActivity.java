package com.ddl.anog;


import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;

public class MainActivity extends Activity implements OnTouchListener{

	public GLSurfaceView glView;
	public boolean isES2 = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
				
		glView = new GLSurfaceView(this);
		glView.setOnTouchListener(this);
		
	

		nativeClass.setAssetsManager(this.getResources().getAssets());
		renderer rend = new renderer(this);
		glView.setRenderer(rend);

		setupJNI();

		
		setContentView(glView);
	}
	
	public native void setupJNI();
	
	public void createExitDialog()
	{
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				showDialog(1);
			}
		});
	}
	
	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
			
		int currentIndex = arg1.getActionIndex();
		int ptrId = arg1.getPointerId(currentIndex);
		
		switch(arg1.getAction() & MotionEvent.ACTION_MASK)
		{
		 case MotionEvent.ACTION_DOWN:
		 {
			 nativeClass.touchDown(ptrId,arg1.getX(currentIndex),arg1.getY(currentIndex));
			 break;
		 }
		 case MotionEvent.ACTION_POINTER_DOWN:
		 {
			 nativeClass.touchDown(ptrId,arg1.getX(currentIndex),arg1.getY(currentIndex));
			 break;
		 }
		 case MotionEvent.ACTION_UP:
		 {
			 nativeClass.touchUp(ptrId,arg1.getX(currentIndex),arg1.getY(currentIndex));
			 break;
		 }
		 case MotionEvent.ACTION_POINTER_UP:
		 {
			 nativeClass.touchUp(ptrId,arg1.getX(currentIndex),arg1.getY(currentIndex));
			 break;
		 }
		 case MotionEvent.ACTION_MOVE:
		 {
			 int count = arg1.getPointerCount();
			 for (int i = 0; i < count; i++)
			 {
				 nativeClass.touchMove(arg1.getPointerId(i),arg1.getX(i),arg1.getY(i));
			 }
			 
			 break;
		 }
		}
		
		return true;
	}
	
    @Override
    protected Dialog onCreateDialog(final int pID) {
            switch(pID) {
                    case 1:
                            return new AlertDialog.Builder(this)
                            .setTitle("Exit")
                            .setMessage("Really exit game?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(final DialogInterface pDialog, final int pWhich) {
                                            finish();
                                    }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(final DialogInterface pDialog, final int pWhich) {
                                    	return;
                                    }
                            })
                            .create();
            }
            return super.onCreateDialog(pID);
    }
}
