#pragma once
#include "asteroid.h"
#include <vector>
#include "game/bulletManager.h"
#include "game/scoresHUD.h"
#include "game/ship.h"
#include "engine/imageManager.h"

namespace game
{
	class asteroidsManager
	{
	public:
		asteroidsManager();
		~asteroidsManager();
		void generateAsteroid();
		void draw(imageManager* imgr);
		void update();
		void checkCollisions();
		void setBulletManager(bulletManager* bMgr);
		void setScoresHUD(scoresHUD*);
		std::vector < asteroid* > getAsteroidList();
		void flushAsteroids(ship*);
	private:
		void addAsteroid(asteroid*);
		void delAsteroid(asteroid*);
		void regenerateAsteroids();
		void createFragments(asteroid*);
		std::vector < asteroid* > asteroids;
		bulletManager* bulletMgr;
		scoresHUD* scHUD;
	};
}