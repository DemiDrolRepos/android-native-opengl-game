#pragma once
#include <vector>
#include "bullet.h"
#include "ship.h"
#include "engine/imageManager.h"

using namespace engine;

namespace game
{
	class bulletManager
	{
	public:
		bulletManager();
		~bulletManager();
		void draw(imageManager* mgr);
		void update();
		void createBullet(ship*);
		std::vector<bullet*> getBulletList();
	private:
		std::vector<bullet*> bullets;
	};

}