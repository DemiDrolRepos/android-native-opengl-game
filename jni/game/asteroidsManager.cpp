#include "asteroidsManager.h"
#include <ctime>
#include "engine/singleton.h"
using namespace game;

asteroidsManager::asteroidsManager()
{
	asteroids.clear();
	bulletMgr = NULL;
	srand(time(0));
	scHUD = NULL;
}

asteroidsManager::~asteroidsManager()
{
	for (size_t i = 0; i < asteroids.size(); i++)
	{
		delete asteroids[i];
	}
	asteroids.clear();
}

void asteroidsManager::draw(imageManager* mgr)
{
	for (size_t i = 0; i < asteroids.size(); i++)
	{
		asteroids[i]->draw(mgr);
	}
}

void asteroidsManager::update()
{
	for (size_t i = 0; i < asteroids.size(); i++)
	{
		asteroids[i]->update();
	}

	regenerateAsteroids();

	//TODO sector collizion
	for (size_t i=0; i < asteroids.size(); i++)
	{
		for (size_t j=0; j < asteroids.size(); j++)
		{
			if (i == j) continue;

			if (asteroids[i]->inRefract() || asteroids[j]->inRefract()) continue;

			if (asteroids[i]->getRect().isIntersect(asteroids[j]->getRect()))
			{
				vector2d iCenter(asteroids[i]->getRect().x + asteroids[i]->getRect().w/2,asteroids[i]->getRect().y + asteroids[i]->getRect().h/2);
				//iCenter.normalize();
				vector2d jCenter(asteroids[j]->getRect().x + asteroids[j]->getRect().w/2,asteroids[j]->getRect().y + asteroids[j]->getRect().h/2);
				//jCenter.normalize();
				vector2d iDestVector = asteroids[i]->getDestination();
				vector2d jDestVector = asteroids[j]->getDestination();
				vector2d n = iCenter - jCenter;
				n.normalize();

				float a1 = iDestVector.dotProduct(n);	
				float a2 = jDestVector.dotProduct(n);		
				float optimizedP = ((a1-a2)* 2.0)/5;
				vector2d v1 = iDestVector - (n*(optimizedP*3)); //todo real mass
				vector2d v2 = jDestVector + (n*(optimizedP*2));
				v1.normalize();
				v2.normalize();
				asteroids[i]->setDestination(v1);
				asteroids[j]->setDestination(v2);
			}
		}
	}

	int count = 0;
	int allc = 0;
	for (size_t i=0; i < asteroids.size(); i++)
	{
		allc++;
		if (asteroids[i]->getHealthLevel() == 3)
			count++;
	}

	if (count < 5)
	{
		generateAsteroid();
	}
}

void asteroidsManager::checkCollisions()
{
	//with bullets
	std::vector<bullet*> bulletList = bulletMgr->getBulletList();
	for (size_t i = 0 ; i <bulletList.size();i++)
	{
		for (size_t j=0; j < asteroids.size(); j++)
		{
			if (bulletList[i]->getRect().isIntersect(asteroids[j]->getRect()))
			{
				bulletList[i]->gotIt();
				asteroids[j]->hit(bulletList[i]->getCenterX(),bulletList[i]->getCenterY());
			}
		}
	}
}

void asteroidsManager::regenerateAsteroids()
{
	checkCollisions();

	std::vector<asteroid*> toDelete;
	for (size_t i=0; i < asteroids.size(); i++)
	{
		if (asteroids[i]->isHited())
		{
			//2 small asteroid etc...
			if (asteroids[i]->getHealthLevel() != 1)
				createFragments(asteroids[i]);

			if (scHUD)
				scHUD->addScore(asteroids[i]->getHealthLevel()*10);

			toDelete.push_back(asteroids[i]);
		}
	}

	for (size_t i=0; i < toDelete.size(); i++)
	{
		for (size_t j=0; j < asteroids.size();j++)
		{
			if (toDelete[i] == asteroids[j])
			{
				asteroids.erase(asteroids.begin() + j);
			}
		}
		delete toDelete[i];
	}
}

void asteroidsManager::createFragments(asteroid* _aster)
{

	asteroid* _firstAster = new asteroid();
	_firstAster->setScale(_aster->getScale()/2);
	_firstAster->setHealthLevel(_aster->getHealthLevel() - 1);

	asteroid* _secondAster = new asteroid();
	_secondAster->setHealthLevel(_aster->getHealthLevel() - 1);
	_secondAster->setScale(_aster->getScale()/2);

	float asterCenterX = _aster->getCoordX() + _aster->getWidth()/2;
	float asterCenterY = _aster->getCoordY() + _aster->getHeight()/2;
	float asterRadius = _secondAster->getWidth()/2;

	float hitX = _aster->getHitX();
	float hitY = _aster->getHitY();
	float angle = 0;

	float directionX = hitX - asterCenterX; 
	float directionY = hitY - asterCenterY;

	if (directionX == 0.0f)
	{
		angle =  (directionY>0.0f) ? 180 : 0;
	} else
	{
		angle = atan(directionY/directionX)*180/M_PI;
		angle = (directionX > 0) ? angle + 90 : angle + 270;
	}

	float fA = angle - 90;
	float sA = angle + 90;
	float firstAsAngle =  (fA-90) * (M_PI/180);
	float firstAsCoordX = asterCenterX + asterRadius*cos(firstAsAngle);
	float firstAsCoordY = asterCenterY + asterRadius*sin(firstAsAngle);

	float secondAsAngle = (sA-90) * (M_PI/180);
	float secondAsCoordX = asterCenterX + asterRadius*cos(secondAsAngle);
	float secondAsCoordY = asterCenterY + asterRadius*sin(secondAsAngle);

	rect astRect = _aster->getRect();
	_firstAster->setCoords(firstAsCoordX - _firstAster->getWidth()/2 ,firstAsCoordY - _firstAster->getHeight()/2);
	_secondAster->setCoords(secondAsCoordX - _secondAster->getWidth()/2,secondAsCoordY - _secondAster->getHeight()/2);
	vector2d dest = _aster->getDestination();
	_firstAster->setDestination(dest);
	_secondAster->setDestination(dest);


	addAsteroid(_firstAster);
	addAsteroid(_secondAster);
}

void asteroidsManager::generateAsteroid()
{
	int randVert = rand() % 4;
	int dW = singleton::getInstance()->getSceneManager()->getDisplayWidth();
	int dH = singleton::getInstance()->getSceneManager()->getDisplayHeight();
	float coordX = 0;
	float coordY = 0;
	float destX = 0;
	float destY = 0;

	switch(randVert)
	{
		case 0: //left
		{
			coordX = -200;
			coordY = 1 +  rand () % dH;
			destX = dW;
			destY = 1 + rand() % dH;
			break;
		}
		case 1: //top
		{
			coordX = 1 + rand() % dW;
			coordY = -200;
			destX = 1 +rand() % dW;
			destY = dH;
			break;
		}
		case 2: // bottom
		{
			coordX = 1 + rand() % dW;
			coordY = dH+200;
			destX = 1 + rand() % dW;
			destY = 1;
			break;
		}
		default:
		case 3:
		{
			coordX = dW + 200;
			coordY = 1 + rand() % dH;
			destX = 1;
			destY = 1 + rand() % dH;
			break; //right
		}
	}

	asteroid* _ast = new asteroid();
	_ast->setCoords(coordX,coordY);
	vector2d dest(destX, destY);
	vector2d src(coordX,coordY);
	dest-=src;
	dest.normalize();
	_ast->setDestination(dest);
	addAsteroid(_ast);
}

void asteroidsManager::addAsteroid(asteroid* _as)
{
	asteroids.push_back(_as);
}

void asteroidsManager::delAsteroid(asteroid* _as)
{
	for (size_t i = 0; i < asteroids.size(); i++)
	{
		if (asteroids[i] == _as)
		{
			asteroids.erase(asteroids.begin() + i);
			return;
		}

	}
}

void asteroidsManager::setScoresHUD(scoresHUD* sc)
{
	scHUD = sc;
}

void asteroidsManager::setBulletManager(bulletManager* bMgr)
{
	bulletMgr = bMgr;
}

std::vector < asteroid* > asteroidsManager::getAsteroidList()
{
	return asteroids;
}

void asteroidsManager::flushAsteroids(ship* _ship)
{
	float flushCenterX = _ship->getPosX() + _ship->getWidth();
	float flushCenterY = _ship->getPosY() + _ship->getHeight();
	float scale =1.0f;
	if (singleton::getInstance()->getSceneManager()->isScaled())
	{
		scale = singleton::getInstance()->getSceneManager()->getScale();
	}

	rect flushRect(flushCenterX - (200*scale),flushCenterY - (200*scale),400*scale,400*scale);
	std::vector<asteroid*> toDelete;
	for (size_t i = 0; i < asteroids.size(); i++)
	{
		if (asteroids[i]->getRect().isIntersect(flushRect))
		{
			toDelete.push_back(asteroids[i]);
		}
	}

	for (size_t i=0; i < toDelete.size(); i++)
	{
		for (size_t j=0; j < asteroids.size();j++)
		{
			if (toDelete[i] == asteroids[j])
			{
				asteroids.erase(asteroids.begin() + j);
			}
		}
		delete toDelete[i];
	}
}