#include "scoresHUD.h"
#include "engine/pngImage.h"
#include <strstream>
#include "engine/singleton.h"

using namespace game;
using namespace engine;
using namespace std;

scoresHUD::scoresHUD()
{
	imageManager* mgr = singleton::getInstance()->getImageManager();
	scoresImage = mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/scores.png");
	scores = 0;
	//for (size_t i=0; i < 9; i++)
	//{
	//	std::ostrstream str;
	//	str.clear();
	//	str << "hud/" << i << ".png" << std::endl;
	//	std::string lstr = str.str();
	//	LOGI(lstr.c_str());
	//	numbers.push_back(new pngImage(lstr));
	//} 
	//android stlports ostream bug????
	numbers.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/numbers/0.png"));
	numbers.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/numbers/1.png"));
	numbers.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/numbers/2.png"));
	numbers.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/numbers/3.png"));
	numbers.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/numbers/4.png"));
	numbers.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/numbers/5.png"));
	numbers.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/numbers/6.png"));
	numbers.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/numbers/7.png"));
	numbers.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/numbers/8.png"));
	numbers.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/numbers/9.png"));
}

scoresHUD::~scoresHUD()
{
	scoresImage->release();
	for (size_t i=0; i < numbers.size(); i++)
	{
		numbers[i]->release();
	}
	numbers.clear();
}

void scoresHUD::draw(imageManager* mgr)
{
	float dW = singleton::getInstance()->getSceneManager()->getDisplayWidth();
	mgr->drawImage(scoresImage,dW-scoresImage->getWidth() - (numbers[0]->getWidth()*1.5f), 0);
	
	int _score = scores;
	int i1 = _score / 100;
	_score -= i1*100;
	int i2 = _score / 10;
	_score -= i2*10;
	int i3 = _score;
	mgr->drawImage(numbers[i1],dW-numbers[0]->getWidth()*2,0);
	mgr->drawImage(numbers[i2],dW-numbers[0]->getWidth()*1.5f,0);
	mgr->drawImage(numbers[i3],dW-numbers[0]->getWidth(),0);
}

void scoresHUD::update()
{

}

void scoresHUD::addScore(int i)
{
	scores += i;
	if (scores >= 999)
		scores = 0;
}

