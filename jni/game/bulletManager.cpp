#include "bulletManager.h"
#include <math.h>

using namespace game;

bulletManager::bulletManager()
{
	bullets.clear();
}

bulletManager::~bulletManager()
{
	for (size_t i=0; i < bullets.size(); i++)
	{
		delete bullets[i];
	}

	bullets.clear();
}

void bulletManager::draw(imageManager* mgr)
{
	for (size_t i = 0; i < bullets.size(); i++)
	{
		bullets[i]->draw(mgr);
	}
}

void bulletManager::update()
{
	std::vector<bullet*> toDelete;
	for (size_t i = 0; i < bullets.size(); i++)
	{
		bullets[i]->update();
		if (bullets[i]->isNeedDelete())
		{
			toDelete.push_back(bullets[i]);
		}
	}

	for (size_t i=0; i < toDelete.size(); i++)
	{
		for (size_t j=0; j < bullets.size(); j++)
		{
			if (toDelete[i] == bullets[j])
				bullets.erase(bullets.begin() + j);
		}
		delete toDelete[i];
	}
	toDelete.clear();
}

void bulletManager::createBullet(ship* _ship)
{
	float angle = (_ship->getRotate() -90) * (M_PI/180); //convert to radians
	float shipCenterX = _ship->getPosX() + _ship->getWidth()/2;
	float shipCenterY = _ship->getPosY() + _ship->getHeight()/2;
	float shipRadius = _ship->getWidth()/2;
	float x = shipCenterX + shipRadius*cos(angle);
	float y = shipCenterY + shipRadius*sin(angle);
	vector2d dst(x,y);
	vector2d src(shipCenterX,shipCenterY);
	dst-=src;
	dst.normalize();


	bullet* _bull = new bullet(x,y, dst);
	bullets.push_back(_bull);
}


std::vector<bullet*> bulletManager::getBulletList()
{
	return bullets;
}