#include "ship.h"
#include "engine/singleton.h"

using namespace game;
using namespace engine;
using namespace std;

ship::ship()
{
	coordX = 0;
	coordY = 0;
	rotateDegrees = 0;
	speedX = 0;
	speedY = 0;
	imageManager* mgr = singleton::getInstance()->getImageManager();
	image = mgr->getSharedImage(sharedImage::TYPE_PNG,"game/ship.png");
	explodeImage = mgr->getSharedImage(sharedImage::TYPE_PNG,"game/explode.png");
	livesCount = 3;
	showExplode = false;
	nextShotTimer = 0;
	nextShotDelta = 50;
	canShootFlag = true;

}

ship::~ship()
{
	image->release();
}

void ship::draw(imageManager* mgr)
{
	mgr->drawImage(image,coordX,coordY,1.0f,rotateDegrees);

	if (showExplode)
	{
		float imageWidth = image->getWidth();
		float imageHeight = image->getHeight();
		float shipCenterX = coordX + imageWidth/2;
		float shipCenterY = coordY + imageHeight/2;
		mgr->drawImage(explodeImage,shipCenterX -explodeImage->getWidth()/2,shipCenterY - explodeImage->getHeight()/2);
	}
	
}

void ship::update(float dT)
{
	coordX += speedX;
	coordY += speedY;

	if (!canShootFlag)
	{
		if (nextShotTimer++ >= nextShotDelta)
		{
			nextShotTimer = 0;
			canShootFlag = true;
		}
	}
}

float ship::getPosX()
{
	return coordX;
}

float ship::getPosY()
{
	return coordY;
}

void ship::setSpeedX(float _speed)
{
	speedX = _speed;
}

void ship::setSpeedY(float _speed)
{
	speedY = _speed;
}

void ship::setPosition(float x, float y)
{
	coordX = x;
	coordY = y;
}

void ship::setDegrees(float deg)
{
	rotateDegrees = deg;
}

bool ship::canShoot()
{
	return canShootFlag;
}

float ship::getRotate()
{
	return rotateDegrees;
}

void ship::shoot()
{
	canShootFlag = false;
}

float ship::getWidth()
{
	return image->getWidth();
}

float ship::getHeight()
{
	return image->getHeight();
}

rect ship::getRect()
{
	float imageWidth = image->getWidth();
	float imageHeight = image->getHeight();
	float rectSizeW = image->getWidth()/2;
	float rectSizeH = image->getHeight()/2;
	float shipCenterX = coordX + imageWidth/2;
	float shipCenterY = coordY + imageHeight/2;

	return rect(shipCenterX-rectSizeW/2,shipCenterY-rectSizeH/2,rectSizeW,rectSizeH);
}