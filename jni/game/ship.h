#pragma once
#include "engine/sharedImage.h"
#include "engine/rect.h"
#include "engine/imageManager.h"

using namespace engine;

namespace game
{
	class ship
	{
	public:
		ship();
		~ship();
		void draw(imageManager* mgr);
		void update(float);
		void setPosition(float,float);
		float getPosX();
		float getPosY();
		float getRotate();
		void setSpeedX(float);
		void setSpeedY(float);
		void setDegrees(float);
		float getWidth();
		float getHeight();
		bool canShoot();
		void shoot();
		rect getRect();
		int getLivesCount() {
			return livesCount;
		};
		void hit()
		{
			livesCount-=1;
		};
		void setShowExplode(bool state)
		{
			showExplode = state; 
		};
	private:
		float coordX;
		float coordY;
		float rotateDegrees;
		float speedX;
		float speedY;
		sharedImage* image;
		int livesCount;
		sharedImage* explodeImage;
		bool showExplode;
		int nextShotTimer;
		int nextShotDelta;
		bool canShootFlag;
	};
}