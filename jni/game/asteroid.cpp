#include "asteroid.h"
#include "engine/singleton.h"

using namespace game;
using namespace engine;

asteroid::asteroid()
{
	speedX = 0.0f;
	speedY = 0.0f;
	coordX = 0.0f;
	coordY = 0.0f;
	rotateDeg = 0.0f;
	rotateSpeed = 0.0f;
	scale = 0.0f;
	healhLevel = 3; 
	imageManager* mgr = singleton::getInstance()->getImageManager();
	image = mgr->getSharedImage(sharedImage::TYPE_PNG,"game/asteroid3.png");
	hitted = false;
	refract = false;
	hitX = 0;
	hitY = 0;
	mass = 1 + (rand() % 10);
}

asteroid::~asteroid()
{
	image->release();
}

void asteroid::setCoords(float x, float y)
{
	coordX = x;
	coordY = y;
}

void asteroid::setRotateSpeed(float rot)
{
	rotateSpeed = rot;
}

float asteroid::getCoordX()
{
	return coordX;
}

float asteroid::getCoordY()
{
	return coordY;
}


void asteroid::setScale(float _scale)
{
	scale = _scale;
}

float asteroid::getScale()
{
	return scale;
}

void asteroid::setHealthLevel(int level)
{
	healhLevel = level;
	if (image)
		image->release();

	imageManager* mgr = singleton::getInstance()->getImageManager();

	if (level == 3)
		image = mgr->getSharedImage(sharedImage::TYPE_PNG,"game/asteroid3.png");

	if (level == 2)
		image = mgr->getSharedImage(sharedImage::TYPE_PNG,"game/asteroid2.png");

	if (level == 1)
		image = mgr->getSharedImage(sharedImage::TYPE_PNG,"game/asteroid1.png");

	//mass = mass/2;
	//if (mass < 1)
	///	mass = 1;
}

int asteroid::getHealthLevel()
{
	return healhLevel;
}

void asteroid::draw(imageManager* mgr)
{
	mgr->drawImage(image,coordX,coordY);
}

void asteroid::update()
{
	coordX += destVector.x;
	coordY += destVector.y;

	checkBounce();

	refract = false;
}

void asteroid::setDestination(vector2d dest)
{
	destVector = dest;
			coordX += destVector.x;
	coordY += destVector.y;
	refract = true;
} 

void asteroid::changeDestination(vector2d vec)
{
	destVector = vec;
		coordX += destVector.x;
	coordY += destVector.y;
	return;
	//refract = true;
	
}

void asteroid::checkBounce()
{
	float dW = singleton::getInstance()->getSceneManager()->getDisplayWidth();
	float dH = singleton::getInstance()->getSceneManager()->getDisplayHeight();
	if (coordX >= dW || coordY >= dH)
	{
		if (coordX >= dW)
		{
			coordX = 0 - image->getWidth() + 1;
		} else
		{
			coordY = 0 - image->getHeight() + 1;
		}

		return;
	} 

	if (coordX + image->getWidth() <= 0 || coordY + image->getHeight() <= 0)
	{
		if (coordX + image->getWidth() <= 0)
		{
			coordX = dW - 1;
		} else
		{
			coordY = dH - 1;
		}
		return;
	}
}

void asteroid::setSpeed(float x, float y)
{
	speedX = x;
	speedY = y;
}

rect asteroid::getRect()
{
	return rect(coordX + 6*healhLevel,coordY + 6*healhLevel,image->getWidth() - 6*healhLevel,image->getHeight() - 6*healhLevel);
}

void asteroid::hit(float _hitX, float _hitY)
{
	hitted = true;
	hitX = _hitX;
	hitY = _hitY;
}

bool asteroid::isHited()
{
	return hitted;
}

vector2d asteroid::getDestination()
{
	return destVector;
}

bool asteroid::inRefract()
{
	return refract;
}

float asteroid::getWidth()
{
	return image->getWidth();
}


float asteroid::getHeight()
{
	return image->getHeight();
}