#pragma once

#include "engine/sharedImage.h"
#include "engine/vector2d.h"
#include "engine/rect.h"
#include "engine/imageManager.h"

using namespace engine;

namespace game
{
	class bullet
	{
	public:
		bullet(float startX,float startY,vector2d dest);
		~bullet();
		void draw(imageManager* mgr);
		void update();
		bool isNeedDelete();
		void gotIt();
		rect getRect();
		float getCenterX() {return currentX;};
		float getCenterY() {return currentY;};
	private:
		float currentX;
		float currentY;
		vector2d destVector;
		sharedImage* image;
		bool needDelete;
	};
}