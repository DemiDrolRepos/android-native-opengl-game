#include "bullet.h"
#include "engine/vector2d.h"
#include "engine/singleton.h"

using namespace game;
using namespace engine;

bullet::bullet(float _startX,float _startY,vector2d dest)
{
	currentX = _startX;
	currentY = _startY;
	imageManager* mgr = NULL;
	mgr = singleton::getInstance()->getImageManager();

	image = mgr->getSharedImage(sharedImage::TYPE_PNG,"game/bullet.png");
	currentX -= image->getWidth()/2;
	currentY -= image->getHeight()/2;
	destVector = dest;
	needDelete = false;
}

bullet::~bullet()
{
	image->release();
	//LOGI("bullet deleted");
}

void bullet::draw(imageManager* mgr)
{
	mgr->drawImage(image,currentX,currentY);
}

void bullet::update()
{
    currentX += destVector.x*4;
    currentY += destVector.y*4;
    float dW = singleton::getInstance()->getSceneManager()->getDisplayWidth();
    float dH = singleton::getInstance()->getSceneManager()->getDisplayHeight();

    if ( (currentX+ image->getWidth() < 0 || currentX > dW) 
    	|| (currentY + image->getHeight() < 0 || currentY > dH) )
    	needDelete = true;
}

bool bullet::isNeedDelete()
{
	return needDelete;
}

rect bullet::getRect()
{
	return rect(currentX,currentY,image->getWidth(),image->getHeight());
}

void bullet::gotIt()
{
	needDelete = true;
}