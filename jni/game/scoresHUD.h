#pragma once

#include "engine/sharedImage.h"
#include "engine/imageManager.h"
#include <vector>

using namespace std;
using namespace engine;

namespace game
{
	class scoresHUD
	{
	public:
		scoresHUD();
		~scoresHUD();
		void draw(imageManager* mgr);
		void update();
		void addScore(int);
	private:
		std::vector<sharedImage*> numbers;
		sharedImage* scoresImage;
		int scores;
	};
}