#include "livesHUD.h"
#include "engine/singleton.h"

using namespace game;
using namespace engine;

livesHUD::livesHUD()
{
	imageManager* mgr = singleton::getInstance()->getImageManager();
	
	shipImageMini = mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/shipMini.png");
	livesCountImage = mgr->getSharedImage(sharedImage::TYPE_PNG,"hud/lives.png");
	_ship = NULL;
}

livesHUD::~livesHUD()
{
	shipImageMini->release();
	livesCountImage->release();
}

void livesHUD::draw(imageManager* mgr)
{
	mgr->drawImage(livesCountImage,0,0);
	if (_ship)
	{
		for (int i=0; i < _ship->getLivesCount(); i++)
		{
			mgr->drawImage(shipImageMini,livesCountImage->getWidth() + (shipImageMini->getWidth() * i),0 + shipImageMini->getWidth()/2);
		}
	}
}

void livesHUD::update()
{

}

void livesHUD::setShip(ship* _sh)
{
	_ship = _sh;
}

