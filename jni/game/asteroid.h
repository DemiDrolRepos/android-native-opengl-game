#pragma once

#include "engine/sharedImage.h"
#include "engine/rect.h"
#include "engine/vector2d.h"
#include "engine/imageManager.h"

using namespace engine;

namespace game
{
	enum eDest
	{
		HORIZONTAL = 0,
		VERTICAL
	};

	class asteroid
	{
	public:
		asteroid();
		~asteroid();
		void setCoords(float,float);
		float getCoordX();
		float getCoordY();
		void setRotateSpeed(float);
		void setDestination(vector2d);
		void setScale(float);
		void setHealthLevel(int);
		void setSpeed(float,float);
		void draw(imageManager*);
		void update();
		void checkBounce();
		void changeDestination(vector2d);
		vector2d getDestination();
		rect getRect();
		void hit(float, float);
		bool isHited();
		bool inRefract();
		int getHealthLevel();
		float getWidth();
		float getHeight();
		float getScale();
		float getHitX() {return hitX;};
		float getHitY() {return hitY;};
		float getMass() {return mass;};
	private:
		float speedX;
		float speedY;
		int mass;
		float coordX;
		float coordY;
		float rotateDeg;
		float rotateSpeed;
		float scale;
		float hitX;
		float hitY;
		int healhLevel; 
		bool hitted;
		sharedImage* image;
		vector2d destVector;
		bool refract;
	};
}