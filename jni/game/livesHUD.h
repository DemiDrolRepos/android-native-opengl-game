#pragma once

#include "engine/sharedImage.h"
#include "engine/imageManager.h"
#include "game/ship.h"

using namespace engine;

namespace game
{
	class livesHUD
	{
		public:
			livesHUD();
			~livesHUD();
			void draw(imageManager* mgr);
			void update();
			void setShip(ship*);
		private:
			sharedImage* shipImageMini;
			sharedImage* livesCountImage;
			ship* _ship;
	};
}