#include <jni.h>

#include <math.h>
#include "defines.h"

#include "sceneManager.h"
#include "imageManager.h"
#include "singleton.h"

static JavaVM* g_JavaVM = NULL;
JNIEnv* env;
jobject ref;
jmethodID exitMethodId;
using namespace engine;

sceneManager* scMgr = NULL;
imageManager* imgr = NULL;

void printError(const char* msg)
{
    int err = glGetError();
    if (err != GL_NO_ERROR )
    {
        LOGI(msg);
        LOGI("%d",err);
    }
}

static void drawFrame()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glLoadIdentity();
    
    scMgr->render();
    //glTranslatef(0.0f, 0.0f,-5.0f);
}

void update(float dT)
{
    scMgr->update(dT);
    imgr->update();
}

void createExitDialog()
{
    env->CallVoidMethod(ref, exitMethodId);
}

static void reinit(int width, int height)
{
    LOGI("Reinit: width = %d, height = %d",width,height);

    glShadeModel(GL_SMOOTH);

    int etaW = 1280;
    float scale = 0.0f;
    if (etaW != width)
    {
        scale = (float)width/(float)etaW;
    }
    //glClearDepthf(1.0f);
    //glEnable(GL_DEPTH_TEST);
    //glDepthFunc(GL_LEQUAL);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glViewport(0, 0, width,height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrthof(0.0, width,height, 0.0f,0.0f, 100.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    //initialize


    imgr = new imageManager();
    scMgr = new sceneManager(width,height);
    scMgr->setExitCallback(createExitDialog);
    if (scale != 0.0f)
    {
        scMgr->setScale(scale); 
    }
    singleton::getInstance()->setSceneManager(scMgr);
    singleton::getInstance()->setImageManager(imgr);
    scMgr->setCurrentScene(scMgr->createScene(sceneManager::SCENE_MAIN_MENU));
}

static void setup()
{

		glEnable(GL_TEXTURE_2D);
		glShadeModel(GL_SMOOTH);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClearDepthf(1.0f);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

static void setAssetsManager(JNIEnv* env, jobject mngr)
{
    singleton::getInstance()->setAssetsManager(AAssetManager_fromJava(env,mngr));
}

static void touchDown(int ptrId, float x, float y)
{
    scMgr->tap(sceneManager::TOUCH_DOWN,ptrId,x,y);
}

static void touchUp(int ptrId, float x, float y)
{
    scMgr->tap(sceneManager::TOUCH_UP,ptrId,x,y);
}

static void touchMove(int ptrId, float x, float y)
{
    scMgr->tap(sceneManager::TOUCH_MOVE,ptrId,x,y);
}


extern "C" {
    JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_drawFrame(JNIEnv * env, jobject obj);
    JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_update(JNIEnv * env, jobject obj,jfloat delta);
    JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_reinit(JNIEnv * env, jobject obj,  jint width, jint height);
    JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_setup(JNIEnv * env, jobject obj);
    JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_setAssetsManager(JNIEnv * env, jobject obj,  jobject mngr);

    //touch

    JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_touchDown(JNIEnv * env, jobject obj,  jint ptrId, jfloat x, jfloat y);
    JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_touchUp(JNIEnv * env, jobject obj,  jint ptrId, jfloat x, jfloat y);
    JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_touchMove(JNIEnv * env, jobject obj,  jint ptrId, jfloat x, jfloat y);

    //exit callback
    JNIEXPORT void JNICALL Java_com_ddl_anog_MainActivity_setupJNI(JNIEnv * env, jobject obj);
}

JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_touchDown(JNIEnv * env, jobject obj,  jint ptrId, jfloat x, jfloat y)
{
    touchDown(ptrId,x,y);
}

JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_touchUp(JNIEnv * env, jobject obj,  jint ptrId, jfloat x, jfloat y)
{
    touchUp(ptrId,x,y);
}

JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_touchMove(JNIEnv * env, jobject obj,  jint ptrId, jfloat x, jfloat y)
{
    touchMove(ptrId,x,y);
}

JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_drawFrame(JNIEnv * env, jobject obj)
{
    drawFrame();
}

JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_reinit(JNIEnv * env, jobject obj,  jint width, jint height)
{
    reinit(width,height);
}

JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_setAssetsManager(JNIEnv * env, jobject obj,  jobject mngr)
{
    setAssetsManager(env,mngr);
}

JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_setup(JNIEnv * env, jobject obj)
{
    setup();
}

JNIEXPORT void JNICALL Java_com_ddl_anog_nativeClass_update(JNIEnv * env, jobject obj,jfloat delta)
{
    update(delta);
}

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
     if (vm->GetEnv((void**) &env, JNI_VERSION_1_6) != JNI_OK)
        return -1;

    return JNI_VERSION_1_6;
}

JNIEXPORT void JNICALL Java_com_ddl_anog_MainActivity_setupJNI(JNIEnv * _env, jobject _obj)
{
   ref = _env->NewGlobalRef(_obj);
   jclass clazz = _env->GetObjectClass(_obj);
   exitMethodId = _env->GetMethodID(clazz, "createExitDialog", "()V");
}
