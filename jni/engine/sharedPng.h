#pragma once
#include "engine/pngImage.h"
#include "engine/singleton.h"
#include "engine/sceneManager.h"

using namespaced engine;

namespace engine
{
	class sharedPng
	{
	public:
		sharedPng(std::string filename)
		{
			sceneManager* _scMgr = singleton::getInstance()->getSceneManager();
			obj = _scMgr->getImage(filename);
			if (!obj)
			{
				obj = new pngImage(filename);
				_scMgr->addImage(filename,obj);		
			}

			increment();
		}
		~sharedPng()
		{
			release();
		}
	private:
		release()
		{
			count--;
			if (count <= 0)
			{
				sceneManager* _scMgr = singleton::getInstance()->getSceneManager();
				obj = _scMgr->delImage(obj);
				delete obj; 
			}
		}
		increment()
		{
			count++;
		}
		int count;
		pngImage* obj;
	};
	sharedPng::count = 0;
}