#pragma once

namespace engine
{
    class scene;
    class sceneManager
    {
    public:
        enum eTouchType
        {
            TOUCH_DOWN = 0,
            TOUCH_UP,
            TOUCH_MOVE
        };

        typedef void (*callback_function)(void); 

        enum eScenes
        {
            SCENE_MAIN_MENU = 0,
            SCENE_GAME,
            SCENE_RESULT
        };

        sceneManager(int , int);
        virtual ~sceneManager();
        void render();
        void update(float dT);
        void tap(eTouchType, int ptrId, float x,float y);
        void setCurrentScene(scene*);
        scene* getCurrentScene();
        scene* createScene(eScenes);
        int getDisplayWidth() {return displayWidth;};
        int getDisplayHeight() {return displayHeight;};
        callback_function exitCallback;
        bool isScaled() {
            return scaled; 
        };
        void setScale(float sc)
        {
            scaled = true;
            globalScale = sc;
        };
        float getScale()
        {
            return globalScale;
        };
        void setExitCallback(callback_function fun)
        {
            exitCallback = fun;
        };
    private:
        scene* currentScene;
        int displayWidth;
        int displayHeight;
        float globalScale;
        bool scaled;
    };

}
