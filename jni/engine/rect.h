#pragma once

class rect
{
public:
	rect(float _x, float _y, float _w, float _h)
	{
		x = _x; y = _y; w = _w; h = _h;
	}

	rect()
	{
		x = y = w = h = 0;
	}

	void setRect(float _x, float _y, float _w, float _h)
	{
		x = _x;
		y = _y;
		w = _w;
		h = _h;
	}

	bool isContain(float _x, float _y)
	{
		if (_x >= x && _x <= x + w)
			if (_y >= y && _y <=  y + h)
				return true;

		return false;
	}

	bool isIntersect(rect r)
	{
		int left1, left2;
		int right1, right2;
		int top1, top2;
		int bottom1, bottom2;

		left1 = x;
		left2 = r.x;
		right1 = x + w;
		right2 = r.x + r.w;
		top1 = y;
		top2 = r.y;
		bottom1 = y + h;
		bottom2 = r.y + r.h;

		if (bottom1 < top2) return false;
		if (top1 > bottom2) return false;

		if (right1 < left2) return false;
		if (left1 > right2) return false;

		return true;
	}

	int x,y,w,h;

};