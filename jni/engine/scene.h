#pragma once
#include <vector>
#include "defines.h"
#include "imageManager.h"

namespace engine
{
    class scene
    {
    public:
        scene();
        virtual ~scene();
        void cleanup();
        virtual void draw(imageManager*);
        virtual void update(float );
        virtual void touchDown(int, float, float);
        virtual void touchUp(int, float, float);
        virtual void touchMove(int, float, float);
    };
}
