#pragma once
#include "sceneManager.h"
#include "defines.h"
#include "imageManager.h"

class singleton
{
private:
	static singleton* instance;
protected:
	singleton() {};
public:
	static singleton* getInstance()
	{
		if (!instance)
			instance = new singleton();
		return instance;
	}

	void setAssetsManager(AAssetManager* mgr)
	{
		asMgr = mgr;
	}

	void setSceneManager(engine::sceneManager* mgr)
	{
		scMgr = mgr;
	}

	void setImageManager(engine::imageManager* mgr)
	{
		imgr = mgr;
	}

	AAssetManager* getAssetsManager()
	{
		return asMgr;
	}

	engine::sceneManager* getSceneManager()
	{
		return scMgr;
	}

	engine::imageManager* getImageManager()
	{
		return imgr;
	}
	engine::sceneManager* scMgr;
	AAssetManager* asMgr;
	engine::imageManager* imgr;
};
