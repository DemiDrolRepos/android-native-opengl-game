#pragma once
#include "defines.h"

#include <string>

namespace engine
{
    class IImage
    {
        //image interface class
    public:
        enum imageType
        {
            BMP = 0,
            PNG, // not impl
            JPG // not impl
        };
        IImage() {};
        virtual ~IImage() {};
        virtual void loadImage(std::string filePath) = 0;
        virtual void draw(float x, float y,float scale,float rotate) = 0;
        virtual float getWidth() = 0;
        virtual float getHeight() = 0;
        virtual void setRotation(float) = 0;
        virtual void setScale(float) = 0;
        virtual std::string getFileName() = 0;
        int nextPow2(int n) 
        { 
         if ( n <= 1 ) return n;
          double d = n-1; 
         return 1 << ((((int*)&d)[1]>>20)-1022); 
        }           

    };
}
