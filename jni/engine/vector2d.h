#pragma once

#include <math.h>

class vector2d
{
public:
	float x;
	float y;
	vector2d() {};
	vector2d(float r, float s)
	{
		x = r;
		y = s;
	}

	vector2d& set(float r, float s)
	{
		x = r;
		y = s;
	}

	float& operator [](long k)
	{
		return ((&x)[k]);
	}

	const float& operator [](long k) const
	{
		return ((&x)[k]);
	}

	vector2d& operator +=(const vector2d& v)
	{
		x += v.x;
		y += v.y;
		return (*this);
	}

	vector2d& operator -=(const vector2d& v)
	{
		x -= v.x;
		y -= v.y;
		return (*this);
	}

	vector2d& operator *=(float t)
	{
		x *= t;
		y *= t;
		return (*this);
	}

	vector2d& operator /=(float t)
	{
		float f = 1.0f / t;
		x *= f;
		y *= f;
		return (*this);
	}

	vector2d& operator *=(const vector2d& v)
	{
		x *= v.x;
		y *= v.y;
		return (*this);
	}

	vector2d operator -(void) const
	{
		return vector2d(-x,-y);
	}

	vector2d operator +(const vector2d& v) const
	{
		return vector2d(x + v.x, y + v.y);
	}

	vector2d operator -(const vector2d& v) const
	{
		return vector2d(x - v.x, y - v.y);
	}

	vector2d operator *(float t) const
	{
		return vector2d(x * t, y * t);
	}

	vector2d operator /(float t) const
	{
		float f = 1.0F /t;
		return (vector2d(x * f, y * f));
	}

	float operator *(const vector2d& v) const
	{
		return (x * v.x + y * v.y);
	}

	vector2d operator &(const vector2d& v) const
	{
		return vector2d(x * v.x,y *v.y);
	}

	bool operator ==(const vector2d& v) const
	{
		return ((x == v.x) && (y == v.y));
	}

	vector2d& normalize(void)
	{
		return (*this /= sqrtf(x*x+y*y));
	}

	float dotProduct(const vector2d& v) const
	{
		return (x * v.x) + (y * v.y);
	}
};
