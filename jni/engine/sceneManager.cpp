#include "sceneManager.h"
#include "scene.h"
#include "scenes/gameScene.h"
#include "scenes/mainMenuScene.h"
#include "scenes/resultScene.h"
#include "defines.h"
#include "singleton.h"
#include "imageManager.h"

using namespace engine;
using namespace game;

sceneManager::sceneManager(int w, int h)
{
    displayWidth = w;
    displayHeight = h;
    currentScene = NULL;
    globalScale = 0.0f;
    scaled = false;
}

sceneManager::~sceneManager()
{
    currentScene = NULL;
}

void sceneManager::render()
{
    imageManager* imgr = singleton::getInstance()->getImageManager();
    if (currentScene)
    {
         currentScene->draw(imgr);
    }
}

void sceneManager::update(float dT)
{
    //LOGI("%f",dT);
    if (currentScene)
        currentScene->update(dT);
}

void sceneManager::tap(sceneManager::eTouchType type,int ptrId,float x, float y)
{
    if (currentScene)
    {
        switch(type)
        {
        case TOUCH_DOWN:
            {
                currentScene->touchDown(ptrId,x,y);
                break;
            }
        case TOUCH_UP:
            {
                currentScene->touchUp(ptrId,x,y);
                break;
            }
        case TOUCH_MOVE:
            {
                currentScene->touchMove(ptrId,x,y);
                break;
            }
        }
    }
}

scene* sceneManager::createScene(eScenes type)
{
    switch(type)
    {
    case SCENE_MAIN_MENU:
        {
            return new mainMenuScene();
        }
    case SCENE_GAME:
        {
            return new gameScene();
        }
    case SCENE_RESULT:
        {
            return new resultScene();
        }
    }

    return NULL;
}

void sceneManager::setCurrentScene(scene* _scene)
{
    if (!_scene)
        return;


    currentScene->cleanup();
    delete currentScene;
    currentScene = _scene;
}


scene* sceneManager::getCurrentScene()
{
    return currentScene;
}
