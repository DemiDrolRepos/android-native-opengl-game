#include "engine/pngImage.h"
#include "engine/rect.h"
#include <string.h>
#include "engine/button.h"
#include "engine/singleton.h"

using namespace std;
using namespace engine;

button::button(std::string filename)
{
	imageManager* mgr = singleton::getInstance()->getImageManager();
	normalImage = mgr->getSharedImage(sharedImage::TYPE_PNG,filename);
	coordX = 0;
	coordY = 0;
}

button::~button()
{
	normalImage->release();
}

void button::draw(imageManager* mgr)
{
	mgr->drawImage(normalImage,coordX,coordY);
}

void button::update()
{

}

void button::touchDown()
{

}

void button::touchUp()
{

}

rect button::getRect()
{
	return imageRect;
}

void button::setCoords(float x, float y)
{
	coordX = x;
	coordY = y;
}

void button::setRectOffsets(float x, float y, float w, float h)
{
	imageRect.setRect(coordX+x,coordY+y,normalImage->getWidth()-w,normalImage->getHeight()-h);
}

float button::getWidth()
{
	normalImage->getWidth();
}

float button::getHeight()
{
	normalImage->getHeight();
}