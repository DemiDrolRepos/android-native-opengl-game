#include "engine/pngImage.h"
#include "engine/bitmapImage.h"
#include "engine/sharedImage.h"
#include <vector>

#pragma once

namespace engine
{
	class imageManager
	{
	public:

		imageManager();
		~imageManager();
		void drawImage(pngImage* img,float x, float y, float scale = 0.0f, float rotate = 0.0f);
		void drawImage(bitmapImage* img, float x, float y, float scale = 0.0f, float rotate = 0.0f);
		void drawImage(sharedImage* img, float x, float y, float scale = 0.0f, float rotate = 0.0f);
		sharedImage* getSharedImage(sharedImage::imageType type,std::string filename);
		void update();
	private:
		std::vector<sharedImage*> sharedImages;
	};
}