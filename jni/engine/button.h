#pragma once
#include "engine/sharedImage.h"
#include "engine/rect.h"
#include "engine/imageManager.h"
#include <string.h>

using namespace std;

namespace engine
{
	class button
	{
	public:
		button(std::string filename);
		~button();
		void draw(imageManager* mgr);
		void update();
		rect getRect();
		void touchDown();
		void touchUp();
		void setCoords(float x, float y);
		float getWidth();
		float getHeight();
		void setRectOffsets(float x, float y, float w, float h);
	private:
		sharedImage* normalImage;
		rect imageRect;
		float coordX;
		float coordY;
	};
}