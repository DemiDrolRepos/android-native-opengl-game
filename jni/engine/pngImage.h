#pragma once

#include "IImage.h"
#include "defines.h"
#include "upng.h"


namespace engine
{
    class pngImage : public IImage
    {
        public:
        pngImage();
        pngImage(std::string filePath);
        virtual ~pngImage();
        void draw(float x, float y,float scale,float rotate);
        void loadImage(std::string filePath);
        float getWidth();
        float getHeight();
        void setRotation(float);
        void setScale(float);
        std::string getFileName() {return filename;};
private:
        bool loaded;
        bool textured;
        GLuint texture[1];
        float width;
        float height;
        float rotation;
        float globalScale;
        std::string filename;
        void createTexture();
        void cleanup();

        upng_t* upng;
    };
}
