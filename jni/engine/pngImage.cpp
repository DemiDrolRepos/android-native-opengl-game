#include "pngImage.h"
#include "defines.h"
#include "singleton.h"

using namespace std;
using namespace engine;


pngImage::pngImage()
{
    loaded = false;
    textured = false;
    rotation = 0;
    globalScale = 0;
}

pngImage::pngImage(std::string filePath)
{
    loaded = false;
    textured = false;
    loadImage(filePath);
    rotation = 0;
    filename = filePath;
    globalScale = 0;
    if (singleton::getInstance()->getSceneManager()->isScaled())
        globalScale = singleton::getInstance()->getSceneManager()->getScale();
}

pngImage::~pngImage()
{
    if (loaded && !textured)
        cleanup();

    if (glIsTexture(texture[0]))
    {
        glDeleteTextures(1,&texture[0]);
        //LOGI("Texture deleted");
    }
}

void pngImage::setRotation(float rot)
{
    rotation = rot;
}

void pngImage::setScale(float sc)
{
    globalScale = sc;
}

void pngImage::createTexture()
{
    if (loaded)
    {
        glGenTextures(1, &texture[0]);
        glBindTexture(GL_TEXTURE_2D, texture[0]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, upng_get_width(upng), upng_get_height(upng), 0, GL_RGBA, GL_UNSIGNED_BYTE, upng_get_buffer(upng));
        //glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        //todo check glIsTexture() && glGetError();
        textured= true;
    } else
    {
        return;
    }

    cleanup();
}

void pngImage::cleanup()
{
    if (loaded && textured)
        upng_free(upng);
}

void pngImage::draw(float x, float y,float scale,float rotate)
{
    if (!loaded && !textured)
        return;

    if (!textured)
    {
        createTexture();
    }

    
    glPushMatrix();
    if (rotate != 0.0f)
    {
        glTranslatef(x + getWidth()/2,y + getHeight()/2,-1.0f);
        glRotatef(rotate,0.0f,0.0f,1.0f);
        glTranslatef(-(x + getWidth()/2),-(y + getHeight()/2),1.0f);
    }

    glBindTexture(GL_TEXTURE_2D, texture[0]);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glFrontFace(GL_CW);

    float scaledWidth = width;
    float scaledHeight = height;
    
    if (globalScale != 0.0f)
    {
        scaledWidth = width * globalScale;
        scaledHeight = height * globalScale;
    } 

    float vertices[] = {
			x, y + scaledHeight,  0.0f,		//bottom left
			x, y,  0.0f,		        // top left
			 x + scaledWidth, y + scaledHeight,  0.0f,		// bottom right
			 x + scaledWidth, y,  0.0f			// top right
	};

	float textures[] = { 

        0.0f,1.0f,
        0.0f,0.0f,
        1.0f,1.0f,
        1.0f,0.0f
	};

    glVertexPointer(3, GL_FLOAT, 0, vertices);
    glTexCoordPointer(2, GL_FLOAT, 0, textures);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    
    glPopMatrix();
    glLoadIdentity();
    
}


void pngImage::loadImage(std::string filePath)
{
    filename = filePath;
    AAssetManager* mgr = singleton::getInstance()->getAssetsManager();
    upng = upng_new_from_file_android(mgr,filePath.c_str());
    upng_decode(upng);
    if (upng_get_error(upng) != UPNG_EOK) {
        LOGI("error: %u %u\n", upng_get_error(upng), upng_get_error_line(upng));
        return;
    }
    width = upng_get_width(upng);
    height = upng_get_height(upng);

    //LOGI("png loaded");
    loaded = true;
}

float pngImage::getWidth()
{
    if (globalScale != 0.0f)
    {
        return width * globalScale;
    } 
    return width;
}

float pngImage::getHeight()
{
    if (globalScale != 0.0f)
    {
        return height * globalScale;
    } 
    return height;
}
