#include "bitmapImage.h"
#include "defines.h"
#include "singleton.h"

using namespace std;
using namespace engine;

bitmapImage::bitmapImage()
{
    pixels = NULL;
    loaded = false;
    textured = false;
    rotation = 0;
    globalScale = 0;
}

bitmapImage::bitmapImage(std::string filePath)
{
    pixels = NULL;
    loaded = false;
    textured = false;
    loadImage(filePath);
    rotation = 0;
    filename = filePath;
    globalScale = 0;
}

bitmapImage::~bitmapImage()
{
    if (loaded && !textured)
        delete [] pixels;

    if (glIsTexture(texture[0]))
    {
        glDeleteTextures(1,&texture[0]);
        //LOGI("Texture deleted");
    }
}

void bitmapImage::setRotation(float rot)
{
    rotation = rot;
}

void bitmapImage::setScale(float sc)
{
    globalScale = sc;
}

void bitmapImage::createTexture()
{
    if (loaded)
    {
        glGenTextures(1, &texture[0]);
        glBindTexture(GL_TEXTURE_2D, texture[0]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        //todo check glIsTexture() && glGetError();
        textured= true;
    } else
    {
        return;
    }

    cleanup();
}

void bitmapImage::cleanup()
{
    if (loaded && textured)
        delete [] pixels;
}


void bitmapImage::draw(float x, float y,float scale,float rotate)
{
    if (!loaded && !textured)
        return;

    if (!textured)
    {
        createTexture();
    }

    
    glPushMatrix();
    if (rotate != 0.0f)
    {
        glTranslatef(x + getWidth()/2,y + getHeight()/2,-1.0f);
        glRotatef(rotation,0.0f,0.0f,1.0f);
        glTranslatef(-(x + getWidth()/2),-(y + getHeight()/2),1.0f);
    }

    //TODO scale bitmap
    glBindTexture(GL_TEXTURE_2D, texture[0]);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glFrontFace(GL_CW);

    


    float vertices[] = {
			x, y + height,  0.0f,		//bottom left
			x, y,  0.0f,		        // top left
			 x + width, y + height,  0.0f,		// bottom right
			 x + width, y,  0.0f			// top right
	};

	float textures[] = { // transponiruem texturu bmp only

        0.0f,0.0f,
        0.0f,1.0f,
        1.0f,0.0f,
        1.0f,1.0f
	};

    glVertexPointer(3, GL_FLOAT, 0, vertices);
    glTexCoordPointer(2, GL_FLOAT, 0, textures);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);




    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glLoadIdentity();
    glPopMatrix();
    
}


void bitmapImage::loadImage(std::string filePath)
{
    filename = filePath;
    AAssetManager* mgr = singleton::getInstance()->getAssetsManager();
    AAsset* fileAsset = AAssetManager_open(mgr, filePath.c_str(), AASSET_MODE_UNKNOWN);
    size_t fileSize = AAsset_getRemainingLength(fileAsset);
    unsigned char temp; // for converting

    BITMAPFILEHEADER fhdr;
    BITMAPINFOHEADER infhdr;

    AAsset_seek(fileAsset, 0, SEEK_SET);
    AAsset_read (fileAsset,&fhdr,sizeof(BITMAPFILEHEADER));

    AAsset_seek(fileAsset, sizeof(BITMAPFILEHEADER), SEEK_SET);
    AAsset_read (fileAsset,&infhdr,sizeof(BITMAPINFOHEADER));

    width = infhdr.biWidth;
    height = infhdr.biHeight;

    AAsset_seek(fileAsset, fhdr.bfOffBits, SEEK_SET);
    pixels = (unsigned char*)malloc(infhdr.biSizeImage);
    AAsset_read (fileAsset,pixels,infhdr.biSizeImage);
    AAsset_close(fileAsset);


    //converting bitmap BGR to RGB
    for  (int imageIdx = 0;imageIdx < infhdr.biSizeImage; imageIdx+=3)
    {
        temp = pixels[imageIdx];
        pixels[imageIdx] = pixels[imageIdx + 2];
        pixels[imageIdx + 2] = temp;
    }

    loaded = true;
}

float bitmapImage::getWidth()
{
    return width;
}

float bitmapImage::getHeight()
{
    return height;
}
