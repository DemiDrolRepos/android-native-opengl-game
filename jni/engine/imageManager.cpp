#include "imageManager.h"

using namespace engine;

imageManager::imageManager()
{
	sharedImages.clear();
	LOGI("1");
}

imageManager::~imageManager()
{
	for (size_t i = 0; i < sharedImages.size(); i++)
	{
		delete sharedImages[i];
	}
}

void imageManager::drawImage(pngImage* img,float x, float y, float scale, float rotate)
{

}

void imageManager::drawImage(bitmapImage* img,float x, float y, float scale, float rotate)
{

}

void imageManager::drawImage(sharedImage* img,float x, float y, float scale, float rotate)
{
	if (img->item == NULL) return;

	img->item->draw(x,y,scale,rotate);
}

sharedImage* imageManager::getSharedImage(sharedImage::imageType type, std::string filename)
{
	for (size_t i=0; i < sharedImages.size(); i++)
	{
		if (sharedImages[i]->item!= NULL && sharedImages[i]->item->getFileName() == filename)
		{
			sharedImages[i]->incrementUsesCount();
			return sharedImages[i];
		}
	}

	// not exists

	sharedImage* image = new sharedImage();
	if (type == sharedImage::TYPE_PNG)
	{
		image->item = new pngImage(filename);
	} else if (type == sharedImage::TYPE_BMP)
	{
		image->item = new bitmapImage(filename);
	}

	image->setType(type);

	sharedImages.push_back(image);

	return image;
}

void imageManager::update()
{
	std::vector<sharedImage*> toDelete;
	for (size_t i=0; i < sharedImages.size(); i++)
	{
		if (sharedImages[i]->getCount() <= 0)
			toDelete.push_back(sharedImages[i]);
	}

	for (size_t i=0; i < toDelete.size(); i++)
	{
		for (size_t j=0; j < sharedImages.size(); j++)
		{
			if (sharedImages[j] == toDelete[i])
			{
				delete sharedImages[j];
				sharedImages.erase(sharedImages.begin() + j);
			}
		}
	}

	toDelete.clear();
}

