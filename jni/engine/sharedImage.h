#pragma once
#include "engine/IImage.h"

namespace engine
{
	class sharedImage
	{
	public:
		enum imageType 
		{ 
			TYPE_PNG = 0,
			TYPE_BMP,
			TYPE_NONE
		};
		sharedImage();
		~sharedImage();
		void release();
		void incrementUsesCount();
		int getCount();
		void setImage(IImage*);
		IImage* getImage();
		IImage* item;
		imageType type;
		imageType getType();
		void setType(imageType);
		float getWidth();
		float getHeight();
	private:
		int count;
	};
}