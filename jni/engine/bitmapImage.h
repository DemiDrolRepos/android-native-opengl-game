#pragma once

#include "IImage.h"
#include "defines.h"


namespace engine
{
    class bitmapImage : public IImage
    {
        public:
        bitmapImage();
        bitmapImage(std::string filePath);
        virtual ~bitmapImage();
        void draw(float x, float y,float scale,float rotate);
        void loadImage(std::string filePath);
        float getWidth();
        float getHeight();
        void setRotation(float);
        void setScale(float);
        std::string getFileName(){return filename;};



        //bitmap structures
        //android(linux) need packed structures
        #pragma pack(1)
        typedef struct tagBITMAPFILEHEADER {
            WORD    bfType;
            DWORD   bfSize;
            WORD    bfReserved1;
            WORD    bfReserved2;
            DWORD   bfOffBits;
        } BITMAPFILEHEADER, *PBITMAPFILEHEADER;

        typedef struct tagBITMAPINFOHEADER{
            DWORD  biSize;
            LONG   biWidth;
            LONG   biHeight;
            WORD   biPlanes;
            WORD   biBitCount;
            DWORD  biCompression;
            DWORD  biSizeImage;
            LONG   biXPelsPerMeter;
            LONG   biYPelsPerMeter;
            DWORD  biClrUsed;
            DWORD  biClrImportant;
        } BITMAPINFOHEADER, *PBITMAPINFOHEADER;
#pragma pack()

private:
        unsigned char* pixels;
        bool loaded;
        bool textured;
        GLuint texture[1];
        float width;
        float height;
        float rotation;
        std::string filename;
        float globalScale;

        void createTexture();
        void cleanup();

    };
}
