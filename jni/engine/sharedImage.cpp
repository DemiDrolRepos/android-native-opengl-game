#include "sharedImage.h"

using namespace engine;

sharedImage::sharedImage()
{
	count = 1;
	item = NULL;
	type = TYPE_NONE;
}

sharedImage::~sharedImage()
{
	if (item)
	{
		delete item;
	}
}

void sharedImage::incrementUsesCount()
{
	count++;
}

void sharedImage::release()
{
	count --;
}

int sharedImage::getCount()
{
	return count;
}

void sharedImage::setImage(IImage* img)
{
	item = img;
}

IImage* sharedImage::getImage()
{
	return item;
}

void sharedImage::setType(imageType ty)
{
	type = ty;
}

sharedImage::imageType sharedImage::getType()
{
	return type;
}

float sharedImage::getWidth()
{
	if (!item) return 0;
	return item->getWidth();
}

float sharedImage::getHeight()
{
	if (!item) return 0;
	return item->getHeight();
}