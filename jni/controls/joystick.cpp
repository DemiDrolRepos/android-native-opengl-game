#include "joystick.h"
#include <math.h>
#include "engine/vector2d.h"
#include "engine/singleton.h"

using namespace engine;

joystick::joystick()
{
	loadImages();
	fingerId = -1;
	knobCoordX = 0;
	knobCoordY = 0;
	baseCoordX = 0;
	baseCoordY = 0;
	agreeRadius = 10;
}

joystick::~joystick()
{
	clearImages();
}

void joystick::draw(imageManager* mgr)
{
	if (fingerId != -1)
	{
		mgr->drawImage(baseImage,baseCoordX,baseCoordY);
		mgr->drawImage(knobImage,knobCoordX,knobCoordY);
	}

}

void joystick::touchDown(int _fingerId, float x, float y)
{
	if (fingerId == -1)
	{
		fingerId = _fingerId;
		baseCoordX = x - baseImage->getWidth() / 2;
		baseCoordY = y - baseImage->getHeight() / 2;
		knobCoordX = x - knobImage->getWidth() / 2;
		knobCoordY = y - knobImage->getHeight() /2;
		agreeRadius = baseImage->getWidth() / 2 + 10;
	}

}

void joystick::touchUp(int _fingerId, float x, float y)
{
	if (fingerId != -1 && fingerId == _fingerId)
	{
		fingerId = -1;
	}
}

void joystick::touchMove(int _fingerId, float x, float y)
{
	if (fingerId != -1 && fingerId == _fingerId)
	{
		float baseCenterX = baseCoordX + baseImage->getWidth() / 2;
		float baseCenterY = baseCoordY + baseImage->getHeight() / 2;

		float d = (float) sqrt(pow(y - baseCenterY, 2) + pow(x - baseCenterX, 2));

		if (d > agreeRadius) {
			float koef = d / agreeRadius;
			x = baseCenterX + (x - baseCenterX) / (koef);
			y = baseCenterY + (y - baseCenterY) / (koef);
		}

		knobCoordX = x - knobImage->getWidth() / 2;
		knobCoordY = y - knobImage->getHeight() / 2;
	}
}

void joystick::loadImages()
{
	imageManager* mgr = singleton::getInstance()->getImageManager();
	baseImage = mgr->getSharedImage(sharedImage::TYPE_PNG,"controls/onscreen_control_base.png");
	knobImage = mgr->getSharedImage(sharedImage::TYPE_PNG,"controls/onscreen_control_knob.png");
}

void joystick::clearImages()
{
	baseImage->release();
	knobImage->release();
}

bool joystick::isActive()
{
	if (fingerId != -1)
		return true;

	return false;
}

float joystick::getAngle()
{
	float knobCenterX = knobCoordX + knobImage->getWidth() / 2;
	float knobCenterY = knobCoordY + knobImage->getHeight() / 2;

	float baseCenterX = baseCoordX + baseImage->getWidth() / 2;
	float baseCenterY = baseCoordY + baseImage->getHeight() / 2;

	float directionX = knobCenterX - baseCenterX; 
	float directionY = knobCenterY - baseCenterY;

	if (directionX == 0.0f) return (directionY>0.0f) ? 180 : 0;

	float rotationAngle = atan(directionY/directionX)*180/M_PI;
	rotationAngle = (directionX > 0) ? rotationAngle + 90 : rotationAngle + 270;
	return rotationAngle;
}

float joystick::getXshift()
{
	float baseCenterX = baseCoordX + baseImage->getWidth() / 2;
	float baseCenterY = baseCoordY + baseImage->getHeight() / 2;
	float knobCenterX = knobCoordX + knobImage->getWidth() / 2;
	float knobCenterY = knobCoordY + knobImage->getHeight() / 2;

	float d = (float) sqrt(pow(knobCenterY - baseCenterY, 2) + pow(knobCenterX - baseCenterX, 2));
	
	if (d == 0.0f) return 0.0f;

	float koef = d / agreeRadius;
	
    vector2d dest(knobCenterX, knobCenterY);
    vector2d src(baseCenterX,baseCenterY);
    dest -= src;
    dest.normalize();
	
	return dest.x * koef;
}	

float joystick::getYshift()
{
	float baseCenterX = baseCoordX + baseImage->getWidth() / 2;
	float baseCenterY = baseCoordY + baseImage->getHeight() / 2;
	float knobCenterX = knobCoordX + knobImage->getWidth() / 2;
	float knobCenterY = knobCoordY + knobImage->getHeight() / 2;

	float d = (float) sqrt(pow(knobCenterY - baseCenterY, 2) + pow(knobCenterX - baseCenterX, 2));
		
	if (d == 0.0f) return 0.0f;

	float koef = d / agreeRadius;

    vector2d dest(knobCenterX, knobCenterY);
    vector2d src(baseCenterX,baseCenterY);
    dest -= src;
    dest.normalize();
		
	return dest.y * koef;
}
