#pragma once
#include "../engine/sharedImage.h"
#include "../engine/imageManager.h"

namespace engine
{
	class joystick
	{
		public:
			joystick();
			~joystick();
			void draw(imageManager* mgr);
			void touchDown(int fingerId, float x, float y);
			void touchUp(int fingerId, float x, float y);
			void touchMove(int fingerId, float x, float y);
			bool isActive();
			float getAngle();
			float getXshift();
			float getYshift();


		private:
			sharedImage* baseImage;
			sharedImage* knobImage;
			float baseCoordX;
			float baseCoordY;
			float knobCoordX;
			float knobCoordY;
			float agreeRadius;
			void loadImages();
			void clearImages();
			int fingerId;
	};
}