#include "resultScene.h"
#include "engine/pngImage.h"
#include "engine/singleton.h"

using namespace game;
using namespace engine;

resultScene::resultScene()
{
	imageManager* mgr = singleton::getInstance()->getImageManager();
	label = mgr->getSharedImage(sharedImage::TYPE_PNG,"resultScene/label.png");

	restartButton = new button("resultScene/restart.png");
    mainMenuButton = new button("resultScene/mm.png");
    
    float dW = singleton::getInstance()->getSceneManager()->getDisplayWidth();
    float dH = singleton::getInstance()->getSceneManager()->getDisplayHeight();
    restartButton->setCoords(dW/2 - 10 - restartButton->getWidth(),dH /2 - restartButton->getHeight()/2);
    restartButton->setRectOffsets(27,39,27,39);
    mainMenuButton->setCoords(dW/2 + 10,dH /2 - mainMenuButton->getHeight()/2);
    mainMenuButton->setRectOffsets(27,39,27,39);
}

resultScene::~resultScene()
{
	label->release();
	delete restartButton;
	delete mainMenuButton;
}

void resultScene::draw(imageManager* mgr)
{
	float dW = singleton::getInstance()->getSceneManager()->getDisplayWidth();
	float dH = singleton::getInstance()->getSceneManager()->getDisplayHeight();
	mgr->drawImage(label,dW/2 - label->getWidth()/2, dH /2 - label->getHeight()/2 - 50);
	restartButton->draw(mgr);
	mainMenuButton->draw(mgr);
}

void resultScene::update(float dT)
{

}

void resultScene::touchDown(int _fingerId, float x, float y)
{

}

void resultScene::touchUp(int _fingerId, float x, float y)
{
    if (mainMenuButton->getRect().isContain(x,y))
    {
        sceneManager* scMgr = singleton::getInstance()->getSceneManager();
        scMgr->setCurrentScene(scMgr->createScene(sceneManager::SCENE_MAIN_MENU));
    }

    if (restartButton->getRect().isContain(x,y))
    {
        sceneManager* scMgr = singleton::getInstance()->getSceneManager();
        scMgr->setCurrentScene(scMgr->createScene(sceneManager::SCENE_GAME));
    }
}

void resultScene::touchMove(int _fingerId, float x, float y)
{

}