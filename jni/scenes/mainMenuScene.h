#pragma once

#include "engine/scene.h"
#include "engine/button.h"
#include <vector>
#include "engine/imageManager.h"
#include "engine/sharedImage.h"

using namespace engine;

namespace game
{
    class mainMenuScene : public engine::scene
    {
        public:
        mainMenuScene();
        ~mainMenuScene();
        void draw(imageManager*);
        void update(float);
        void touchDown(int _fingerId, float x, float y);
        void touchUp(int _fingerId, float x, float y);
        void touchMove(int _fingerId, float x, float y);
    private:
        void createButtons();
        void deleteButtons();
        sharedImage* background;
        button* _playButton;
        button* _exitButton;
        sharedImage* sun;
        std::vector<sharedImage*> stars;
        std::vector< std::pair<int,int> > starCoords;
        float starShift;
        float shiftSpeed;
        int shiftKoef;

    };
}