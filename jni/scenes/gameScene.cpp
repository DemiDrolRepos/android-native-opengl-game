#include "gameScene.h"
#include "../controls/joystick.h"
#include "engine/singleton.h"
#include <vector>

using namespace game;
using namespace engine;

gameScene::gameScene()
{
    joy = new joystick();

    int width = singleton::getInstance()->getSceneManager()->getDisplayWidth();
    int height = singleton::getInstance()->getSceneManager()->getDisplayHeight();
    joystickRect.setRect(0,0,width*0.5,height);
    shotRect.setRect(width-width*0.5f,0,width,height);

    _ship = new ship();
    _ship->setPosition(width / 2 - _ship->getWidth()/2, height / 2 - _ship->getHeight()/2);
    asterManager = new asteroidsManager();
        
    for (size_t i=0; i < 7; i++)
    {
         asterManager->generateAsteroid();
    }

    bulletMgr = new bulletManager();
    asterManager->setBulletManager(bulletMgr); // for bullet collisions
    _shipHUD = new livesHUD();
    _shipHUD->setShip(_ship);

    _scores = new scoresHUD();
    asterManager->setScoresHUD(_scores);
    pause = false;
    pauseTimer = 0;
    imageManager* mgr = singleton::getInstance()->getImageManager();

    movementArea = mgr->getSharedImage(sharedImage::TYPE_PNG,"controls/movement_area.png");
    shootingArea = mgr->getSharedImage(sharedImage::TYPE_PNG,"controls/shooting_area.png");

}

gameScene::~gameScene()
{
    delete _ship;
    delete asterManager;
    delete joy;
    delete bulletMgr;
    delete _shipHUD;
    movementArea->release();
    shootingArea->release();
}

void gameScene::draw(imageManager* mgr)
{
    int width = singleton::getInstance()->getSceneManager()->getDisplayWidth();
    int height = singleton::getInstance()->getSceneManager()->getDisplayHeight();
    mgr->drawImage(movementArea,0,height-movementArea->getHeight());
    mgr->drawImage(shootingArea,width - shootingArea->getWidth(),height-shootingArea->getHeight());
    asterManager->draw(mgr);
    _ship->draw(mgr);

    bulletMgr->draw(mgr);
    joy->draw(mgr);
    _shipHUD->draw(mgr);
    _scores->draw(mgr);
}

void gameScene::touchDown(int _fingerId, float x, float y)
{
    if(joystickRect.isContain(x,y))
    {
        joy->touchDown(_fingerId,x,y);
    }

    if (shotRect.isContain(x,y))
    {

        if (_ship->canShoot())
        {
            bulletMgr->createBullet(_ship);
            _ship->shoot();
        }
    }
}

void gameScene::touchUp(int _fingerId, float x, float y)
{
    //if(joystickRect.isContain(x,y))
   // {
        joy->touchUp(_fingerId,x,y);
   // }
}

void gameScene::touchMove(int _fingerId, float x, float y)
{
   // if(joystickRect.isContain(x,y))
   // {
        joy->touchMove(_fingerId,x,y);
   // }
}

void gameScene::update(float dT)
{
	if (pause)
	{
		if (pauseTimer++ >= 50)
		{
			pause = false;
			_ship->setShowExplode(false);
		}

		return;
	}
    if (joy->isActive())
    {
        float x = joy->getAngle();
        _ship->setSpeedX(joy->getXshift()*2);
        _ship->setSpeedY(joy->getYshift()*2);  
        _ship->setDegrees(joy->getAngle());    
    }
    _ship->update(dT); 
    asterManager->update();
    std::vector<asteroid*> list = asterManager->getAsteroidList();
	for (size_t i=0; i < list.size(); i++)
	{
		rect _rect = list[i]->getRect();
		if (_rect.isIntersect(_ship->getRect()))
		{
			_ship->hit();
			_ship->setShowExplode(true);
			pause=true;
			pauseTimer = 0;
			if (_ship->getLivesCount() <= 0)
			{
				sceneManager* scMgr = singleton::getInstance()->getSceneManager();
        		scMgr->setCurrentScene(scMgr->createScene(sceneManager::SCENE_RESULT));
				return;
			} else
			{
				asterManager->flushAsteroids(_ship);
				return;
			}
		}
	}
    bulletMgr->update();
    _shipHUD->update();
}

