#include "controls/joystick.h"
#include "game/ship.h"
#include "engine/rect.h"
#include "game/bulletManager.h"
#include "game/asteroidsManager.h"
#include "engine/scene.h"
#include "game/livesHUD.h"
#include "game/scoresHUD.h"
#include "engine/imageManager.h"
#include "engine/sharedImage.h"

namespace engine
{
	class joystick;
    class sharedImage;
}

namespace game
{
    class gameScene: public engine::scene
    {
    public:
        gameScene();
        ~gameScene();
        void draw(imageManager*);     
        void touchDown(int _fingerId, float x, float y);
        void touchUp(int _fingerId, float x, float y);
        void touchMove(int _fingerId, float x, float y);
        void update(float );
    private:
        engine::joystick* joy;
        sharedImage* movementArea;
        sharedImage* shootingArea;
        rect joystickRect;
        rect shotRect;
        ship* _ship;
        asteroidsManager* asterManager;
        bulletManager* bulletMgr;
        float rotate;
        livesHUD* _shipHUD;
        scoresHUD* _scores;
        bool pause;
        int pauseTimer;
    };
}


