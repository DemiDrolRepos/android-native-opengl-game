#pragma once
#include "engine/scene.h"
#include "engine/sharedImage.h"
#include "engine/button.h"
#include "engine/imageManager.h"

using namespace engine;

namespace game
{
    class resultScene: public engine::scene
    {
    public:
        resultScene();
        ~resultScene();
        void draw(imageManager*);
        void update(float);
        void touchDown(int _fingerId, float x, float y);
        void touchUp(int _fingerId, float x, float y);
        void touchMove(int _fingerId, float x, float y);
    private:
    	sharedImage* label;
    	button* restartButton;
    	button* mainMenuButton;
    };
}
