#include "mainMenuScene.h"
#include "engine/singleton.h"

using namespace game;
using namespace engine;

mainMenuScene::mainMenuScene()
{
    createButtons();
    imageManager* mgr = singleton::getInstance()->getImageManager();
    sun = mgr->getSharedImage(sharedImage::TYPE_PNG,"stars/sun.png");

    float scale = 1.0f;
    if (singleton::getInstance()->getSceneManager()->isScaled())
        scale = singleton::getInstance()->getSceneManager()->getScale();

    stars.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"stars/red_star_medium.png"));
    starCoords.push_back(std::make_pair<int,int> (417*scale,69*scale));

    stars.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"stars/red_star_medium.png"));
    starCoords.push_back(std::make_pair<int,int> (1208*scale,666*scale));

    stars.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"stars/red_star_light.png"));
    starCoords.push_back(std::make_pair<int,int> (769*scale,91*scale));


    stars.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"stars/red_star_heavy.png"));
    starCoords.push_back(std::make_pair<int,int> (272*scale,584*scale));

    stars.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"stars/green_star_medium.png"));
    starCoords.push_back(std::make_pair<int,int> (1090*scale,99*scale));

    stars.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"stars/green_star_light.png"));
    starCoords.push_back(std::make_pair<int,int> (932*scale,555*scale));

    stars.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"stars/green_star_heavy.png"));
    starCoords.push_back(std::make_pair<int,int> (384*scale,384*scale));

    stars.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"stars/gray_star_medium.png"));
    starCoords.push_back(std::make_pair<int,int> (1150*scale,238*scale));

    stars.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"stars/gray_star_light.png"));
    starCoords.push_back(std::make_pair<int,int> (499*scale,149*scale));

    stars.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"stars/gray_star_light.png"));
    starCoords.push_back(std::make_pair<int,int> (484*scale,622*scale));

    stars.push_back(mgr->getSharedImage(sharedImage::TYPE_PNG,"stars/gray_star_heavy.png"));
    starCoords.push_back(std::make_pair<int,int> (81*scale,345*scale));

    starShift = 0.0f;
    shiftKoef = 1;
    shiftSpeed = 0.05f;
}

mainMenuScene::~mainMenuScene()
{
    deleteButtons();
    sun->release();
    for (size_t i=0; i < stars.size(); i++)
    {
        if (stars[i])
            stars[i]->release();
    }
    stars.clear();  
}

void mainMenuScene::draw(imageManager* mgr)
{
    for (size_t i=0; i < stars.size(); i++)
    {
        mgr->drawImage(stars[i],starCoords[i].first-stars[i]->getWidth()/2 + starShift,starCoords[i].second-stars[i]->getHeight()/2);
    }

    mgr->drawImage(sun,0-sun->getWidth()/2,0-sun->getHeight()/2);

    _playButton->draw(mgr);
    _exitButton->draw(mgr);
}

void mainMenuScene::update(float dT)
{
    starShift+= shiftSpeed;
    if (starShift > 20.0f || starShift < -20.0f)
        shiftSpeed *= -1;

}

void mainMenuScene::createButtons()
{
    _playButton = new button("mainmenu/buttons/play.png");
    _exitButton = new button("mainmenu/buttons/exit.png");
    float dW = singleton::getInstance()->getSceneManager()->getDisplayWidth();
    float dH = singleton::getInstance()->getSceneManager()->getDisplayHeight();
    float scale = 1.0f;
    if (singleton::getInstance()->getSceneManager()->isScaled())
        scale = singleton::getInstance()->getSceneManager()->getScale();
    _playButton->setCoords(dW/2 -_playButton->getWidth()/2, dH/2 - _playButton->getHeight());
    _playButton->setRectOffsets(14*scale,7*scale,14*scale,7*scale);
    _exitButton->setCoords(dW/2 -_exitButton->getWidth()/2, dH/2);
    _exitButton->setRectOffsets(14*scale,7*scale,14*scale,7*scale);
}

void mainMenuScene::deleteButtons()
{
    delete _playButton;
    delete _exitButton;
}

void mainMenuScene::touchDown(int _fingerId, float x, float y)
{

}

void mainMenuScene::touchUp(int _fingerId, float x, float y)
{
    if (_playButton->getRect().isContain(x,y))
    {
        sceneManager* scMgr = singleton::getInstance()->getSceneManager();
        scMgr->setCurrentScene(scMgr->createScene(sceneManager::SCENE_GAME));
    }

    if (_exitButton->getRect().isContain(x,y))
    {
        sceneManager* scMgr = singleton::getInstance()->getSceneManager();
        scMgr->exitCallback();
    }
}

void mainMenuScene::touchMove(int _fingerId, float x, float y)
{

}