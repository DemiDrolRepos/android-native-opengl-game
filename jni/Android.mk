LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := libnativeclass
LOCAL_CFLAGS    := -Werror

LOCAL_SRC_FILES :=  engine/nativeClass.cpp \
                    engine/sceneManager.cpp \
                    engine/scene.cpp \
                    engine/bitmapImage.cpp \
                    scenes/gameScene.cpp \
                    scenes/mainMenuScene.cpp \
                    scenes/resultScene.cpp \
                    controls/joystick.cpp \
                    engine/singleton.cpp \
                    game/ship.cpp \
                    game/asteroid.cpp \
                    game/asteroidsManager.cpp \
                    game/bulletManager.cpp \
                    game/bullet.cpp \
                    engine/upng.cpp \
                    engine/pngImage.cpp \
                    engine/button.cpp \
                    game/livesHUD.cpp \
                    game/scoresHUD.cpp \
                    engine/imageManager.cpp \
                    engine/sharedImage.cpp



LOCAL_LDLIBS    := -llog -lGLESv1_CM -landroid

include $(BUILD_SHARED_LIBRARY)
